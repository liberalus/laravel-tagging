<?php

declare(strict_types=1);

namespace App\Providers;

use App\Modules\Report\Reports\CostReport;
use App\Modules\Report\Reports\ReportHandler;
use App\Modules\Report\Reports\SaleReport;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->tag([SaleReport::class, CostReport::class], 'reports');

        $this->app->bind(ReportHandler::class, function ($app) {
            return new ReportHandler($app->tagged('reports'));
        });

//        $this->app->bind(ReportHandler::class, function ($app) {
//            return new ReportHandler(...$app->tagged('reports'));
//        });

//        $this->app->when(ReportHandler::class)
//            ->needs('$reports')
//            ->giveTagged('reports');
    }
}
