<?php

declare(strict_types=1);

namespace App\Modules\Report;

use RuntimeException;

class ReportException extends RuntimeException
{

}
