<?php

declare(strict_types=1);

namespace App\Modules\Report\Reports;

class CostReport implements ReportInterface
{
    public function getName(): string
    {
        return 'cost';
    }

    public function getReport(): array
    {
        return [];
    }
}
