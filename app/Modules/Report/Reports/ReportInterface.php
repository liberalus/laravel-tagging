<?php

declare(strict_types=1);

namespace App\Modules\Report\Reports;

interface ReportInterface
{
    public function getName(): string;

    public function getReport(): array;
}
