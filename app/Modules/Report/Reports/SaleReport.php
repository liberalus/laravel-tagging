<?php

declare(strict_types=1);

namespace App\Modules\Report\Reports;

class SaleReport implements ReportInterface
{
    public function getName(): string
    {
        return 'sale';
    }

    public function getReport(): array
    {
        return [
            10.0,
            89.7,
        ];
    }
}
