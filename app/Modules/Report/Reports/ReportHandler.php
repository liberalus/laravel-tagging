<?php

declare(strict_types=1);

namespace App\Modules\Report\Reports;

use App\Modules\Report\ReportException;
use Illuminate\Container\RewindableGenerator;
use Illuminate\Contracts\Auth\Guard;

class ReportHandler
{
    private array $reports;

    public function __construct(RewindableGenerator $reports)
    {
        foreach ($reports->getIterator() as $report) {
            $this->reports[$report->getName()] = $report;
        }
    }

//    public function __construct(ReportInterface ...$reports)
//    {
//        foreach ($reports as $report) {
//            $this->reports[$report->getName()] = $report;
//        }
//    }

//    public function __construct(array $reports, Guard $guard)
//    {
//        foreach ($reports as $report) {
//            $this->reports[$report->getName()] = $report;
//        }
//    }

    public function getReport(string $name): ReportInterface
    {
        throw_unless(isset($this->reports[$name]), new ReportException('Report nor found'));

        return $this->reports[$name];
    }
}
