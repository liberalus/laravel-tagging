<?php

declare(strict_types=1);

namespace App\Modules\Report;

use App\Modules\Report\Reports\ReportHandler;
use App\Modules\Report\Reports\ReportInterface;

class ReportService
{
    private ReportHandler $reportHandler;

    public function __construct(ReportHandler $reportHandler)
    {
        $this->reportHandler = $reportHandler;
    }

    public function getReport(string $name): ReportInterface
    {
        return $this->reportHandler->getReport($name);
    }
}
