<?php

declare(strict_types=1);

namespace App\Modules\Report;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;

class ReportController
{
    private ReportService $reportService;

    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
    }

    public function show(string $name): JsonResponse
    {
        $report = $this->reportService->getReport($name);

        return new JsonResponse([
            'title' => Str::ucfirst($report->getName()),
            'data' => $report->getReport(),
        ]);
    }
}
