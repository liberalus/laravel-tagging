<?php

use App\Modules\Report\ReportController;
use Illuminate\Support\Facades\Route;

Route::get('/report/{name}', [ReportController::class, 'show']);
